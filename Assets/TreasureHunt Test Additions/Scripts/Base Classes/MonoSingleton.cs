﻿/// <summary>
/// Mono singleton.
/// Created by Jorge L. Chavez Herrera.
/// 
/// Gerenric singleton class
/// Derive all singleton objects from MonoSingleton.
/// </summary>
using UnityEngine;
using System.Collections;

namespace TreasureHunt.Test.Framework
{

	public abstract class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
	{
		#region Class accessors
		private static T _instance = null;
		public static T Instance
		{
			get
			{
				// If Instance requiered for the first time ...
				if ( _instance == null )
				{
					// Intance initializations
					_instance = GameObject.FindObjectOfType(typeof(T)) as T;
					
					if ( FindObjectsOfType(typeof(T)).Length > 1 )
					{
						Debug.LogError ("[Singleton "+typeof(T)+"] error, more than 1 instance found!");
						return _instance;
					}
					
					// Object not found, we create a temporary one
					if( _instance == null )
					{
						_instance = new GameObject ("Temp Instance of " + typeof(T).ToString(), typeof(T)).GetComponent<T>();
						
						// Problem during the creation, this should not happen
						if( _instance == null )
						{
							Debug.LogError("Error during the creation of " + typeof(T).ToString());
						}
					}
					_instance.Init();
				}
				return _instance;
			}
		}
		#endregion
		
		#region MonoBehaviour overrides
		// If no other monobehaviour request the instance in an awake function
		// executing before this one, no need to search the object.
		private void Awake ()
		{
			if ( _instance == null )
			{
				_instance = this as T;
				_instance.Init();
			}
		}
		
		// This function is called when the instance is used the first time
		// Put all the initializations you need here, as you would do in Awake.
		public virtual void Init () {}
		
		// Removes the instance when the object is destroyed.
		public virtual void OnDestroy()
		{
			_instance = null;
		}
		#endregion
	}
}