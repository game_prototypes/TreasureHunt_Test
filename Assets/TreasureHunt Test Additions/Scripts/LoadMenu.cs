﻿/// <summary>
/// Load Menu.
/// Provides an UI interface for loading game state saves
/// On this very first implementation a thumbnail is dsplayed  
///
/// Created by Jorge L. Chavez Herrera.
/// </summary>

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TreasureHunt.Test.Framework;


public class LoadMenu : MonoBehaviour 
{
	#region Class members
	public Canvas pauseMenuCanvas; 		// A reference to the pause menu canvas
	public Canvas canvas;				// A reference to this menu canvas
	public RawImage gameStateThumbnail; // A reference to a raw image widget to show the gamestate thumbnail
	public PauseManager pauseMamnager;  // A reference to the pause manager 
	public Button loadButton;
	#endregion
	
	#region Class implementation
	/// <summary>
	/// Show this The load menu panel.
	/// </summary>
	public void Show ()
	{
		GameStateManager.Instance.LoadFromPlayerPrefs ();
		gameStateThumbnail.texture = GameStateManager.Instance.GetSavedGameStateThumbnail ();
		canvas.enabled = true;
		
		// Disable the load button if theres no saved game state
		loadButton.interactable = GameStateManager.Instance.GameStateSaved ();
	}
	
	/// <summary>
	/// Loads the state of the game.
	/// </summary>
	public void LoadGameState ()
	{
		GameStateManager.Instance.LoadFromPlayerPrefs ();
		GameStateManager.Instance.RestoreGameState ();
		canvas.enabled = false;
		pauseMamnager.Pause ();
		
	}
	
	/// <summary>
	/// Cancels loading a gamestate and hides this panel
	/// </summary>
	/// <returns><c>true</c> if this instance cancel ; otherwise, <c>false</c>.</returns>
	public void Cancel ()
	{
		pauseMenuCanvas.enabled = true;
	}
	#endregion
}
