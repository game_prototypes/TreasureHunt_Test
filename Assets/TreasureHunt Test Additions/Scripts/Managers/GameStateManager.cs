﻿/// <summary>
/// Game State Manager.
/// Singleton class providing fucntionality for saving & restoring game state.
///
/// Created by Jorge L. Chavez Herrera.
/// </summary>

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using TreasureHunt.Test.Framework;
using UnityEngine.Audio;

namespace TreasureHunt.Test.Framework
{
	public class GameStateManager : MonoSingleton<GameStateManager> 
	{
		#region Class members
		public PauseMenu pauseMenu;
		public AudioMixer masterMixer; // A reference to the master AudioMixer.
		private GameState gameState;   // Structure for holding current game state.
		#endregion
		
		#region MonoSingleton overrides
		/// <summary>
		/// Initializes this instance.
		/// </summary>
		override public void Init ()
		{
			gameState = new GameState ();
		}
		
		void Update ()
		{
			if (Input.GetKeyDown (KeyCode.Alpha0))
				PlayerPrefs.DeleteAll ();
		}
		#endregion
		
		#region Class implementation
		
		/// <summary>
		/// Captures the state of the game.
		/// </summary>
		public void CaptureGameState ()
		{
			gameState.CaptureGameplayState ();
			gameState.CaptureSoundPreferences (pauseMenu);
			gameState.SetThumbnail (ScreenGrab.screenGrab);
		}
		
		/// <summary>
		/// Restores the state of the game.
		/// </summary>
		public void RestoreGameState ()
		{
			ClearGameScene ();
			gameState.RestoreGameplayState ();
			gameState.RestoreSoundPreferences (pauseMenu);
			Debug.Log ("Game State restore successfully.");
		}
		
		/// <summary>
		/// Clears the game scene.
		/// </summary>
		public void ClearGameScene ()
		{
			CompleteProject.EnemyHealth[] enemiesHealth = GameObject.FindObjectsOfType<CompleteProject.EnemyHealth>();
			
			// Destroy all current enemies in scene
			for (int i = 0; i < enemiesHealth.Length; i++)
			{
				Destroy (enemiesHealth[i].gameObject);
			}	
		}
		
		public Texture2D GetSavedGameStateThumbnail ()
		{
			Texture2D thumbnail = new Texture2D (280, 180);
			thumbnail.LoadImage (gameState.thumbnail);
			
			return thumbnail;
		}
		#endregion

		#region XML Serialization
		/// <summary>
		/// Converts an UTF8Byte array to string.
		/// </summary>
		/// <returns>The f8 byte array to string.</returns>
		/// <param name="characters">Characters.</param>
		static public string UTF8ByteArrayToString (byte[] characters)	
		{
			UTF8Encoding encoding = new UTF8Encoding();
			string constructedString = encoding.GetString(characters);
			
			return (constructedString);
		}
		
		/// <summary>
		/// Converts a string to a UTF8Byte array.
		/// </summary>
		/// <returns>The to UT f8 byte array.</returns>
		/// <param name="pXmlString">P xml string.</param>
		static public byte[] StringToUTF8ByteArray (string pXmlString) 
		{
			UTF8Encoding encoding = new UTF8Encoding();
			byte[] byteArray = encoding.GetBytes(pXmlString);
			return byteArray;
		} 
		
		
		/// <summary>
		/// Uses XML to serializa an object returning a string.
		/// </summary>
		/// <returns>The object.</returns>
		/// <param name="pObject">P object.</param>
		static public string SerializeObject (object pObject)
		{
			string XmlizedString = null;
			MemoryStream memoryStream = new MemoryStream ();
			
			XmlSerializer xs = new XmlSerializer (typeof (GameState));
			XmlTextWriter xmlTextWriter = new XmlTextWriter(memoryStream, Encoding.UTF8);
			xs.Serialize(xmlTextWriter, pObject);
			memoryStream = (MemoryStream)xmlTextWriter.BaseStream;
			XmlizedString = UTF8ByteArrayToString (memoryStream.ToArray());
			return XmlizedString;	
		}
		
		/// <summary>
		/// Deserializes an XML serialized string to an object.
		/// </summary>
		/// <returns>The object.</returns>
		/// <param name="pXmlizedString">P xmlized string.</param>
		object DeserializeObject (string pXmlizedString) 
		{
			XmlSerializer xs = new XmlSerializer (typeof (GameState));
			MemoryStream memoryStream = new MemoryStream (StringToUTF8ByteArray (pXmlizedString));
			return xs.Deserialize (memoryStream);
		}
		
		/// <summary>
		/// Saves current game state to player prefs
		/// </summary>
		public void SaveToPlayerPrefs () 
		{
			string gameProgressString = SerializeObject (gameState);
			PlayerPrefs.SetString ("GameState", SerializeObject (gameState));
			
			Debug.Log ("Gamer progress saved to player prefs");
		}
		
		/// <summary>
		/// Loads game state from player prefs
		/// </summary>
		public void LoadFromPlayerPrefs () 
		{
			if (PlayerPrefs.HasKey ("GameState"))
			{
				string gameProgressString = PlayerPrefs.GetString ("GameState");
				gameState = (GameState)DeserializeObject (gameProgressString);
			}
		}
		
		/// <summary>
		/// Retunrs true iof theres a game state saved
		/// </summary>
		/// <returns><c>true</c>, if state saved was gamed, <c>false</c> otherwise.</returns>
		public bool GameStateSaved ()
		{
			return PlayerPrefs.HasKey ("GameState");
		}
		
		#endregion
	}
}
