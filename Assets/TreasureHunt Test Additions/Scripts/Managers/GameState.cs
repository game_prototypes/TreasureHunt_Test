﻿/// <summary>
/// Agent state.
/// Stores relevant game state information for agents.
///
/// Created by Jorge L. Chavez Herrera.
/// </summary>

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;

namespace TreasureHunt.Test.Framework
{
	[System.Serializable]
	public class GameState 
	{
		#region Class members
		public int score;					 // The player score
		public AgentState playerState; 		 // The player state
		public List<AgentState> enemyStates; // All alive enemy states in scene
		public Vector3 cameraPosition;		 // Gameplay camera position
		
		public float musicVolume;			// Music volume
		public float sfxVolume;				// SFX volume 
		public bool audioEnabled;			// Audio on/off
		
		public byte[] thumbnail;
		#endregion
		
		#region Class implementation
		/// <summary>
		/// Default constructor
		/// Initializes a new instance of the <see cref="TreasureHunt.Test.Framework.GameState"/> class.
		/// </summary>
		public GameState ()
		{
			score = 0;
			playerState = new AgentState();;
			enemyStates = new List<AgentState> ();
			cameraPosition = Vector3.zero;
			musicVolume = 1;
			sfxVolume = 1;
			audioEnabled = true;
		}
		
		/// <summary>
		/// Captures the state of the animation time.
		/// </summary>
		/// <param name="go">Go.</param>
		/// <param name="agentState">Agent state.</param>
		private static void CaptureAnimationState (GameObject go, ref AgentState agentState)
		{
			Animator animator = go.GetComponent<Animator> ();
			
			if (animator != null)
			{
				AnimatorStateInfo currentState = animator.GetCurrentAnimatorStateInfo (0);
				agentState.animationTime = currentState.normalizedTime % 1;
			}
		}
		
		/// <summary>
		/// Captures the state of the player.
		/// </summary>
		/// <returns>The player state.</returns>
		/// <param name="playerHealth">Player health.</param>
		private static AgentState CapturePlayerState (CompleteProject.PlayerHealth playerHealth)
		{
			AgentState newPlayerState = new AgentState (playerHealth.name, playerHealth.currentHealth, "", 0, playerHealth.transform);
			
			return newPlayerState;
		}
		
		/// <summary>
		/// Captures the state of an enemy.
		/// </summary>
		/// <returns>The enemy state.</returns>
		/// <param name="enemyHealth">Enemy health.</param>
		private static AgentState CaptureEnemyState (CompleteProject.EnemyHealth enemyHealth)
		{
			AgentState newEnemyState = new AgentState (enemyHealth.name, enemyHealth.currentHealth, "", 0, enemyHealth.transform);
			
			return newEnemyState;
		}
		
		/// <summary>
		/// Capture the current game state.
		/// </summary>
		public void CaptureGameplayState ()
		{
			// Capture the player's score
			score = CompleteProject.ScoreManager.score;
			
			// Caputure the state of the player
			CompleteProject.PlayerHealth playerHealth = GameObject.FindObjectOfType<CompleteProject.PlayerHealth>();
			playerState = CapturePlayerState (playerHealth);
			//CaptureAnimationState (playerHealth.gameObject, ref playerState);
					
			// Capture teh state of all currently alive enemies in scene
			enemyStates.Clear ();
			CompleteProject.EnemyHealth[] enemiesHealth = GameObject.FindObjectsOfType<CompleteProject.EnemyHealth>();	
			
			for (int i = 0; i < enemiesHealth.Length; i++)
			{
				// We will only save the state for alive agents
				if (enemiesHealth[i].currentHealth > 0)
				{
					AgentState enemyState = CaptureEnemyState (enemiesHealth[i]);
					//CaptureAnimationState (enemiesHealth[i].gameObject, ref enemyState);
					enemyStates.Add (enemyState);	
				}
			}
			
			// Capture camera position
			cameraPosition = Camera.main.GetComponent<Transform>().position;
		}
		
		/// <summary>
		/// Restore the current game state.
		/// </summary>
		public void RestoreGameplayState ()
		{
			// Restore the player's score
			CompleteProject.ScoreManager.score = score;
			
			// Restore player state
			CompleteProject.PlayerHealth playerHealth = GameObject.FindObjectOfType<CompleteProject.PlayerHealth>();
			playerHealth.GetComponent<CompleteProject.PlayerHealth>().SetCurrentHealth (playerState.health);
			playerHealth.GetComponent<Transform>().position = playerState.position;
			playerHealth.GetComponent<Transform>().eulerAngles = new Vector3 (0, playerState.yAngle,0);
			
			// restore enemy states
			for (int i = 0; i < enemyStates.Count; i++)
			{
				AgentState enemyState = enemyStates[i]; // For best readability.
				
				GameObject prefab = Resources.Load (enemyState.name) as GameObject;
				
				if (prefab != null)
				{
					GameObject go = GameObject.Instantiate (prefab); 
					go.name = enemyState.name;
					go.GetComponent<CompleteProject.EnemyHealth>().currentHealth = enemyState.health;
					go.GetComponent<Transform>().position = enemyState.position;
					go.GetComponent<Transform>().eulerAngles = new Vector3 (0, enemyState.yAngle,0);
				}
				else
					Debug.LogError ("Error: the prefab for the saved agent state was nof found!");
			}
			
			// Capture camera position
			Camera.main.GetComponent<Transform>().position = cameraPosition;
		}
		
		/// <summary>
		/// Captures the sound preferences.
		/// </summary>
		/// <param name="masterMixer">Master mixer.</param>
		public void CaptureSoundPreferences (PauseMenu pauseMenu)
		{
			musicVolume = pauseMenu.musicVolumeSlider.normalizedValue;
			sfxVolume = pauseMenu.sfxVolumeSlider.normalizedValue;
			audioEnabled = Camera.main.GetComponent<AudioListener>().enabled;
		}
		
		/// <summary>
		/// Restores the sound preferences.
		/// </summary>
		/// <param name="masterMixer">Master mixer.</param>
		public void RestoreSoundPreferences (PauseMenu pauseMenu)
		{
			pauseMenu.UpdateSoundWidgets (musicVolume, sfxVolume, audioEnabled);
		}
		
		/// <summary>
		/// Sets the game state thumbnail.
		/// </summary>
		/// <param name="thumbnail">Thumbnail.</param>
		public void SetThumbnail (Texture2D thumbnail)
		{
			this.thumbnail = thumbnail.EncodeToPNG ();
		}
		
		#endregion
	}
}