﻿/// <summary>
/// Agent state.
/// Stores relevant game state information for agents.
///
/// Created by Jorge L. Chavez Herrera.
/// </summary>

using UnityEngine;
using System.Collections;

namespace TreasureHunt.Test.Framework
{
	[System.Serializable]
	public class AgentState
	{
		#region Class members
		public string name; 		 // The name of the agent (it also refers to the prefabs name this agent was instatntiated from)
		public int health; 			 // The health for the agent
		public string animationName; // The name for the current animation state
		public float animationTime;  // The time for the current animation being played on this agnet
		public Vector3 position;     // The world position for the agent
		public float yAngle;		 // The y euler angle for this agent
		#endregion
		
		#region Class implementation
		/// <summary>
		/// Default constructor
		/// Initializes a new instance of the <see cref="TreasureHunt.Test.Framework.AgentState"/> class.
		/// </summary>
		public AgentState ()
		{
			this.name = null;
			this.health = 0;
			this.animationName = null;
			this.animationTime = 0;
			this.position = Vector3.zero;
			this.yAngle = 0;
		}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="TreasureHunt.Test.Framework.AgentState"/> class.
		/// </summary>
		/// <param name="name">Name.</param>
		/// <param name="health">Health.</param>
		/// <param name="animationName">Animation name.</param>
		/// <param name="animationTime">Animation time.</param>
		/// <param name="transform">Transform.</param>
		public AgentState (string name, int health, string animationName, float animationTime, Transform transform)
		{
			this.name = name;
			this.health = health;
			this.animationName = animationName;
			this.animationTime = animationTime;
			this.position = transform.position;
			this.yAngle = transform.eulerAngles.y; 
		}
		
		/// <summary>
		/// Returns a string representation for this agent state
		/// </summary>
		/// <returns>A <see cref="System.String"/> that represents the current <see cref="TreasureHunt.Test.Framework.AgentState"/>.</returns>
		public string ToString ()
		{
			return string.Format ("Name: {0}, Health: {1}, State name: {2}, AnimationTime: {3}, WorldPosition ({4}), Y Angle: {5:0.0}", 
			                      name, health, animationName, animationTime, position.ToString (), yAngle);
		}
		#endregion
	}
}
	