﻿/// <summary>
/// Screen Grab
///
/// Grabs the screen in to a texture, this function is slow but since the game will be paused when capturing game state thumbnails, 
/// we don't care about performance.
/// Additionally, this class uses a temporary camera a quad and a render texture to resize the desired image
///
/// Created by Jorge L. Chavez Herrera.
/// </summary>
using UnityEngine;
using System.Collections;


public class ScreenGrab : MonoBehaviour 
{
	#region Class members
	private bool mustGrab; 				// flag, set to true for screen cgrabbing
	static public Texture2D screenGrab; // The latest screen grab
	#endregion
	
	#region MonoBehaviour overrides
	/// <summary>
	/// Sets the flag for screen grabbing.
	/// </summary>
	public void Grab ()
	{
		mustGrab = true;
	}
	
	void OnPostRender() 
	{
		if (mustGrab) 
		{
			screenGrab = new Texture2D (Screen.width, Screen.height);
			screenGrab.ReadPixels (new Rect(0, 0, Screen.width, Screen.height), 0, 0);
			screenGrab.Apply ();
			screenGrab = ResizeTexture (screenGrab, 240, 240);
			mustGrab = false;
			Debug.Log ("Screen Grabbed");
		}
	}
	
	/// <summary>
	/// Uses a temporary camera a quad and a render texture to resize the desired image
	/// </summary>
	/// <returns>The texture.</returns>
	/// <param name="texture">Texture.</param>
	/// <param name="width">Width.</param>
	/// <param name="height">Height.</param>
	static public Texture2D ResizeTexture (Texture2D texture, int width, int height) 
	{
		// Setup a temporary render texture
		RenderTexture renderTexture = RenderTexture.GetTemporary (width, height);
		RenderTexture.active = renderTexture;
		
		// Set up the a temporary camera to render with
		GameObject cameraGo = new GameObject("Temp Camera");
		Camera tempCam = cameraGo.AddComponent<Camera>();
		
		tempCam.targetTexture = renderTexture;
		tempCam.orthographic = true;
		tempCam.near = -10;
		tempCam.far = 10;
		tempCam.orthographicSize = 0.25f;
		//tempCam.aspect = (float)height / (float)width;
		tempCam.clearFlags = CameraClearFlags.Color;
		tempCam.backgroundColor = Color.black;
		tempCam.cullingMask = 1 << LayerMask.NameToLayer("TempCamera");
		tempCam.GetComponent<Transform>().localPosition = new Vector3(0,0,0f);
		
		// Create a quad to render the resized screen grab
		GameObject quadGO = GameObject.CreatePrimitive(PrimitiveType.Quad);
		quadGO.layer = LayerMask.NameToLayer("TempCamera");
		quadGO.transform.position = Vector3.zero;
		quadGO.transform.rotation = Quaternion.identity;
		Material unlitMaterial = new Material (Shader.Find ("Unlit/Texture"));
		quadGO.GetComponent<Renderer>().material = unlitMaterial;
		quadGO.GetComponent<Renderer>().material.SetTexture ("_MainTex", screenGrab);
		
		// Render	
		tempCam.Render();
		
		// Setup the texture
		Texture2D resizedImage = new Texture2D (width, height, TextureFormat.RGB24, false);
		resizedImage.ReadPixels (new Rect(0,0,width,height),0,0);
		resizedImage.Apply ();	 
		resizedImage.filterMode = FilterMode.Bilinear;
		
		// Cleanup
		RenderTexture.ReleaseTemporary (renderTexture);
		Destroy (cameraGo);
		Destroy (quadGO);
		
		return resizedImage;
	}
	#endregion
	
}
