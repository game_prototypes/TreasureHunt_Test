﻿/// <summary>
/// Pause Menu.
/// Provides an interface to interact with the pause menu panel.
///
/// Created by Jorge L. Chavez Herrera.
/// </summary>

using UnityEngine;
using System.Collections;
using TreasureHunt.Test.Framework;
using UnityEngine.UI;

namespace TreasureHunt.Test.Framework
{
	public class PauseMenu : MonoBehaviour
	{
		#region Class members
		public LoadMenu loadMenu;
		public Slider musicVolumeSlider; // A reference to the music volume slider
		public Slider sfxVolumeSlider;	 // A reference to the sfx volume slider
		public Toggle audioToggle;		 // A reference to the audio toggle
		#endregion
		
		#region Class implementation
		/// <summary>
		/// Saves the state of the game.
		/// </summary>
		public void SaveGameState ()
		{
			GameStateManager.Instance.CaptureGameState ();
			GameStateManager.Instance.SaveToPlayerPrefs ();
		}
		
		/// <summary>
		/// Restores the state of the game.
		/// </summary>
		public void RestoreGameState ()
		{
			loadMenu.Show ();
		}
		
		/// <summary>
		/// Updates the sound widgets.
		/// </summary>
		/// <param name="musicVolume">Music volume.</param>
		/// <param name="sfxVolume">Sfx volume.</param>
		/// <param name="audioOn">If set to <c>true</c> audio on.</param>
		public void UpdateSoundWidgets (float musicVolume, float sfxVolume, bool audioOn)
		{
			musicVolumeSlider.normalizedValue = musicVolume;
			sfxVolumeSlider.normalizedValue = sfxVolume;
			audioToggle.isOn = audioOn;
		}
		#endregion
	}
}
