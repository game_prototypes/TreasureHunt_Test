﻿Treasure Hunt Work Test
By Jorge L. Chavez Herrera.

Notes:

- The Unity project lacks of a real game structure.

- No pool manager found for instancing enemies, theres some hickups when starting the game because of this.

- There was a big mess finding the demo classes because all scripts are duplicate within the project, updated ones for Unity 5 reside under the name space CompleteProject, but this is barely documented.

- Implemented GameState Save Load Functionality and XML serialization.

- Serialized Game Satet is saved to player prefs for now, althought this is NOT a good idea for biiger projects because of PlayerPrefs file size limitations, 
  this is a good starting point for testing and demo purposes.
  
- Missing Data Encription due to time constrains.

- There was enough time to implement saving the full game state plus Sound preferences.

- There was also enough time to implement grabbing the screen right before pause and then creating a thumnail image for the saved game state.

- Moved some prefabs to resources folder to load and instantiate prefabs when restoring the game state,

- TODO

- Save multiple game states.

- Implement Data encription.

- With a more robust implementation Enemy State could be tracked from the spawners.